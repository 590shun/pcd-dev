import open3d as o3d
import numpy as np
import sys

def annotate_point_cloud(pcd_path, num_classes):
    pcd = o3d.io.read_point_cloud(pcd_path)
    colors = np.asarray(pcd.colors)
    if colors.shape[1] != 3:
        colors = np.zeros((len(pcd.points), 3))

    labels = np.zeros((len(pcd.points),))
    default_colors = np.copy(colors)

    for i in range(num_classes):
        print(f"\nAnnotating for class {i+1}/{num_classes}. Please select a region with the mouse.")
        selected_points = []
        while not selected_points:
            _, selected_points = o3d.visualization.draw_geometries_with_rectangular_region_picking([pcd])
        
        label_color = np.random.rand(3)
        for idx in selected_points:
            labels[idx] = i + 1
            colors[idx] = label_color

        pcd.colors = o3d.utility.Vector3dVector(colors)
    
    o3d.visualization.draw_geometries([pcd])
    return labels

num_classes = int(input("アノテーションを行うクラスの数: "))
file_name = sys.argv[1]
labels = annotate_point_cloud(file_name, num_classes)
print(labels)
