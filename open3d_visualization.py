import open3d as o3d
import numpy as np
import sys

class PointPicker:
    def __init__(self, pcd):
        self.pcd = pcd
        self.vis = o3d.visualization.VisualizerWithEditing()
        self.vis.create_window()
        self.vis.add_geometry(self.pcd)
        self.points = []

    def pick_points(self):
        self.vis.run()  # Start the visualization
        self.points = self.vis.get_picked_points()
        return self.points

def compute_distance(point1, point2):
    diff = np.array(point1) - np.array(point2)
    return np.linalg.norm(diff)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script_name.py <path_to_pointcloud>")
        exit()

    filepath = sys.argv[1]
    pcd = o3d.io.read_point_cloud(filepath)

    picker = PointPicker(pcd)
    picker.pick_points()

    if len(picker.points) == 2:
        point1 = pcd.points[picker.points[0]]
        point2 = pcd.points[picker.points[1]]
        distance = compute_distance(point1, point2)
        print(f"Distance between {point1} and {point2}: {distance}")
    else:
        print("Please select exactly two points.")
