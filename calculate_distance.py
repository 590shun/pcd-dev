import numpy as np
import open3d as o3d
import sys
import matplotlib.pyplot as plt

file_name = sys.argv[1]

def pick_points(pcd):
    print("")
    print("1) Please pick at least two points using [shift + left click]")
    print("   Press [shift + right click] to undo point picking")
    print("2) After picking points, press 'Q' to close the window")
    vis = o3d.visualization.VisualizerWithEditing()
    vis.create_window()
    vis.add_geometry(pcd)
    vis.run()  # User picks points
    vis.destroy_window()
    print("")
    return vis.get_picked_points()

def main():
    # Load point cloud data
    pcd = o3d.io.read_point_cloud(file_name)  # Change to the path of your file
    
    # Visualize the point cloud and pick points
    picked_points = pick_points(pcd)
    
    if len(picked_points) < 2:
        print("You need to select at least two points!")
        return
    
    # Compute the distance between the two picked points
    point1 = np.asarray(pcd.points[picked_points[0]])
    point2 = np.asarray(pcd.points[picked_points[1]])
    distance = np.linalg.norm(point1 - point2)

    # Compute the midpoint between the two points
    midpoint = (point1 + point2) / 2

    # Create a small point cloud for the midpoint
    colors = np.array([[0, 1, 0]])  # green color for the midpoint
    points = np.array([midpoint])
    mid_pcd = o3d.geometry.PointCloud()
    mid_pcd.points = o3d.utility.Vector3dVector(points)
    mid_pcd.colors = o3d.utility.Vector3dVector(colors)

    # Create a LineSet to visualize the line between the points
    line_set = o3d.geometry.LineSet(
        points=o3d.utility.Vector3dVector([point1, point2]),
        lines=o3d.utility.Vector2iVector([[0, 1]])
    )

    print(f"Distance between points: {distance:.2f}")

    # Visualize the point cloud, line, and midpoint with the distance
    geometries = [pcd, line_set, mid_pcd]
    o3d.visualization.draw_geometries(geometries)

if __name__ == '__main__':
    main()